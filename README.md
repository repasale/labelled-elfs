# Labelled Elfs

This repository contains 100/100 x86_64 log runs of benignware and malware through strace.
The labelled elfs were collected from the repository [labeled-elfs on github](https://github.com/nimrodpar/Labeled-Elfs).

Feel free to use
